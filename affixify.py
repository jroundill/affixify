import sublime, sublime_plugin

class AffixifyCommand(sublime_plugin.TextCommand):

	def run(self, edit):
		for sel in self.view.sel():
			s = prefix + self.view.substr(sel) + suffix
			self.view.replace(edit, sel, s)

	def __init__(self, view):
		self.view = view
		global prefix
		global suffix
		prefix = ""
		suffix = ""

	def setup_prefix(self):
		self.view.window.show_input_panel("Prefix:", '', self.on_submit_prefix, None, None)

	def setup_suffix(self):
		self.view.window.show_input_panel("Suffix:", '', self.on_submit_suffix, None, None)

	def on_submit_prefix(self, text):
		global prefix
		prefix = text
		print text

	def on_submit_suffix(self, text):
		global suffix
		suffix = text
		print text


class AffixifySetupPrefixCommand(sublime_plugin.WindowCommand):
	def run(self):  
		a = AffixifyCommand(self).setup_prefix();

class AffixifySetupSuffixCommand(sublime_plugin.WindowCommand):
	def run(self):  
		a = AffixifyCommand(self).setup_suffix();